# Programmer Assistant HASS Add-on: Registry

The Registry is a stateless, highly scalable server side application that stores and lets you distribute Docker images.
This addon is preconfigured to serve docker registry cache server.

## Installation

The installation of this add-on is pretty straightforward and not different in
comparison to installing any other Home Assistant add-on.

1. Search for the "Docker Registry" add-on in the Supervisor add-on store and install it.
2. Set `REGISTRY_PROXY_USERNAME` and `REGISTRY_PROXY_PASSWORD`
3. Start the "Docker registry" add-on.
4. Configure your docker deamon to use cache [link].
5. Check the logs of the "Docker registry".

## Configuration

**Note**: _Remember to restart the add-on when the configuration is changed._

Example add-on configuration:

```yaml
env_vars:
  - name: REGISTRY_PROXY_REMOTEURL
    value: https://registry-1.docker.io
  - name: REGISTRY_PROXY_USERNAME
    value: abc
  - name: REGISTRY_PROXY_PASSWORD
    value: xyz
ssl: true
certfile: fullchain.pem
keyfile: privkey.pem
```

### Option: `env_vars`

This option allows you to tweak every aspect of docker registry by setting
configuration options using environment variables. See the example at the
start of this chapter to get an idea of how the configuration looks.

For more information about using these variables, see the official docker registry
documentation:

<https://docs.docker.com/registry/configuration/>

**Note**: _Only environment variables starting with `REGISTRY_` are accepted.\_

## Changelog & Releases

This repository keeps a change log using [CHANGELOG.md](CHANGELOG.md)
functionality.

Releases are based on [Semantic Versioning][semver], and use the format
of `MAJOR.MINOR.PATCH`. In a nutshell, the version will be incremented
based on the following:

- `MAJOR`: Incompatible or major changes.
- `MINOR`: Backwards-compatible new features and enhancements.
- `PATCH`: Backwards-compatible bugfixes and package updates.

## Support

Got questions?

[Open an issue here][issue] on GitLab.

## Authors & contributors

The original setup of this repository is by [Dawid Rycerz][knightdave].

For a full list of all authors and contributors,
check [the contributor's page][contributors].

## License

MIT License

Copyright (c) 2021-2021 Dawid Rycerz

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

[semver]: http://semver.org/spec/v2.0.0.html
[issue]: https://gitlab.com/programmer-assistant/addons/-/issues
[comminuty-addons]: https://addons.community
[frenck]: https://github.com/frenck
[contributors]: https://gitlab.com/programmer-assistant/addons/-/graphs/master
[knightdave]: https://gitlab.com/knightdave
[link]: https://docs.docker.com/registry/recipes/mirror/#configure-the-docker-daemon
