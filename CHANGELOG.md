# Changelog

# [0.3.0](https://gitlab.com/programmer-assistant/addon-registry/compare/v0.2.0...v0.3.0) (2021-09-19)


### :arrow_up:

* Update image path and upgrade base image ([bce39e2](https://gitlab.com/programmer-assistant/addon-registry/commit/bce39e22eb0885da358a73d20803aaacfeb400a6))

## 0.2.0

- rename config file to fix supervisior warning

## 0.1.4

- add default config

## 0.1.3

- fix path to registry binary

## 0.1.2

- hide passwords in console
- fix registry binary architecture

## 0.1.1

- Fix mounting ssl

## 0.1.0

- Initial docker registry addon setup based on registry 2.7.1
